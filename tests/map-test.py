import sys, json, os, random

sys.path.append( os.path.join( os.path.dirname( __file__ ), '..', 'map' ) )
import map as Map

sys.path.append( os.path.join( os.path.dirname( __file__ ), '..', 'data' ) )

try:
    with open( os.path.join( os.path.dirname( __file__ ), '..', 'data', "game_data.json"), "r" ) as game_data_file:
        game_data = json.load( game_data_file )
except:
    print( "File Not Found" )

    game_data = {
        "volume": 5.0,
        "combat_speed": 1,
        "monster_spawn_limit": 5,
        "alive_monster": []
    }


try:
    with open( os.path.join( os.path.dirname( __file__ ), '..', 'data', "player_data.json"), "r" ) as player_data_file:
        player_data = json.load( player_data_file )
except:
    print( "File Not Found" )

    player_data = {
        "name": "Null",
        "maxhp": 100,
        "hp": 100,
        "pos": [ 500, 300 ],
        "movement": 25
    }


player = Map.Hero( player_data )
#player.setName( "Steve" )
#player.setStatus( "alive" )

game = Map.Game( game_data )

#game.setMonsterSpawnLimit( random.randint( 4, 8 ) )
#Map.getNewSpawn( game, player_data )

Map.loadGame( game, player )
Map.titleScreen( game, player )
