import pygame, time, sys, random

pygame.init()
pygame.font.init()

display_height = 600
display_width = 800
screen = pygame.display.set_mode( (display_width, display_height) ) # width and height
done = False

clock = pygame.time.Clock()

## COLORS

black = (0,0,0)
white = (255,255,255)

red = (200,0,0)
green = (0,200,0)

bright_red = (255,0,0)
bright_green = (0,255,0)


## SOUNDS
game_ost = "manaplus/icecontinent.ogg"


def Greetings():
    screen.fill(black)
    myfont = pygame.font.SysFont('Comic Sans MS', 30)

    #pygame.draw.rect( <surface instance>, (<r>,<g>,<b>), pygame.Rect( <x>,<y>,<height>,<width> ) ) x and y of top left corner
    pygame.draw.rect( screen, ( 0, 128, 255 ), pygame.Rect( 0, display_height-80, display_width, 100 ) )

    textsurface = myfont.render( 'Greetings...', False, (255, 255, 255) )
    screen.blit(textsurface,( 0, display_height-80 ))

            
    pygame.display.flip()


def Goodbye():
    myfont = pygame.font.SysFont('Comic Sans MS', 30)

    pygame.display.flip()

    #pygame.draw.rect( <surface instance>, (<r>,<g>,<b>), pygame.Rect( <x>,<y>,<height>,<width> ) ) x and y of top left corner
    pygame.draw.rect( screen, ( 255, 0, 0 ), pygame.Rect( 0, display_height-80, display_width, 100 ) )

    textsurface = myfont.render( 'Farewell...', False, (255, 255, 255) )
    screen.blit(textsurface,( 0, display_height-80 ))

            
    pygame.display.flip()


def textBox( sentence, color=( 255, 0, 0 ), font='Comic Sans MS' ):
    pygame.font.init()
    myfont = pygame.font.SysFont(font, 30)

    #pygame.draw.rect( <surface instance>, (<r>,<g>,<b>), pygame.Rect( <x>,<y>,<height>,<width> ) ) x and y of top left corner
    pygame.draw.rect( screen, color, pygame.Rect( 0, display_height-80, display_width, 100 ) )

    textsurface = myfont.render( sentence, False, (255, 255, 255) )
    screen.blit(textsurface,( 0, display_height-80 ))

            
    pygame.display.flip()


def textObjects(text, font):
    textSurface = font.render(text, True, black)
    return textSurface, textSurface.get_rect()


def dialogue( sentence_list ):
    try:
        textBox( sentence_list[0] )
        del(sentence_list[0])
    except:
        rand_int = random.randint(0, 2)
        textBox( nothing_more[rand_int], color=( 0, 128, 255 ) )

def titleScreen():
    intro = True

    while intro:
        for event in pygame.event.get():
            #print(event)
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
    
        screen.fill(white)
        largeText = pygame.font.SysFont("Comic Sans MS",115)
        TextSurf, TextRect = textObjects("A bit Racey", largeText)
        TextRect.center = (round(display_width/2),round(display_height/2))
        screen.blit(TextSurf, TextRect)

        button("Start", 150, 450, 100, 50, green, bright_green, Greetings)
        button("Quit", 550, 450, 100, 50, red, bright_red, Quit)

        pygame.display.update()
        clock.tick(15)


def button(msg,x,y,w,h,ic,ac,action=None):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x+w > mouse[0] > x and y+h > mouse[1] > y:
        pygame.draw.rect(screen, ac,(x,y,w,h))
        if click[0] == 1 and action != None:
            action()         
    else:
        pygame.draw.rect(screen, ic,(x,y,w,h))
        
    smallText = pygame.font.SysFont("comicsansms",20)
    textSurf, textRect = textObjects(msg, smallText)
    textRect.center = ( round(x+(w/2)), round(y+(h/2)) )
    screen.blit(textSurf, textRect)


def Quit():
    pygame.display.quit()
    pygame.quit()
    sys.exit(); #sys.exit() if sys is imported
    

dialogue_list = [
    "I have not seen visitors in a very long time....",
    "I wonder... what do you think I am?",
    "Never mind. That is not important right now..."
    ]
   
nothing_more = [
    "I have nothing more to say.",
    "Please leave me alone...",
    "What do you want?"
    ]  

temp = []

titleScreen()

pygame.mixer.music.set_volume( 0.5 )
pygame.mixer.music.load(game_ost)
pygame.mixer.music.play(-1)

while True:
    for event in pygame.event.get(): # Event Listener
        keys = pygame.key.get_pressed()
		
        if event.type == pygame.QUIT:
            Quit()
			
        elif event.type == pygame.KEYDOWN: # checks for keyboard input		
            #if keys[pygame.K_UP]:
            #    Greetings()
	#		
	#					
         #   elif keys[pygame.K_DOWN]:
          #      Goodbye()

            dialogue( dialogue_list )

    
