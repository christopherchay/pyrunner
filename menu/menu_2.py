import pygame, time, sys, random, os

pygame.init()

height = 600
width = 800

"""
ASPECT RATIOS

Default
    height = 600
    width = 800

Large
    height = 700
    width = 1100

"""


screen = pygame.display.set_mode( (width, height) ) # width and height
done = False

clock = pygame.time.Clock()

## SOUNDS
game_ost = "manaplus/icecontinent.ogg"
home_screen_st = "manaplus/woodland_fantasy.ogg"


## Loading Screens
home_screen_bg = pygame.image.load( os.path.join("MLBB", "yu_zhong_SWP.jpg") ) # 600x450


## VARS
volume = 0.6
fade_time = 500 # ms

def Greetings():
    pygame.font.init()
    myfont = pygame.font.SysFont('Comic Sans MS', 30)

    #pygame.draw.rect( <surface instance>, (<r>,<g>,<b>), pygame.Rect( <x>,<y>,<height>,<width> ) ) x and y of top left corner
    pygame.draw.rect( screen, ( 0, 128, 255 ), pygame.Rect( 0, height-80, width, 100 ) )

    textsurface = myfont.render( 'Greetings...', False, (255, 255, 255) )
    screen.blit(textsurface,( 0, height-80 ))

            
    pygame.display.flip()

def main_menu():
    pygame.mixer.music.set_volume( volume )
    pygame.mixer.music.load(game_ost)
    pygame.mixer.music.play(-1)
    
    click = False
    
    while True:
        screen.fill( (0, 0, 0) )
        
        pygame.font.init()
        myfont = pygame.font.SysFont('Comic Sans MS', 60)
        button_font = pygame.font.SysFont('Comic Sans MS', 20)
        
        mx, my = pygame.mouse.get_pos()

        textsurface = myfont.render( 'A Tale of 2 Cities', False, (255, 255, 255) )
        screen.blit(textsurface,( int(width/2-250), int(height/2-200) ))

        button1 = pygame.Rect( 200, 400, 100, 50 ) # ( x, y, width, height )
        button2 = pygame.Rect( 500, 400, 100, 50 )

        if button1.collidepoint( (mx, my) ):
            if click:
                pygame.mixer.music.fadeout(fade_time)
                home_menu()
        if button2.collidepoint( (mx, my) ):
            if click:
                Quit()

        pygame.draw.rect( screen, (0, 255, 0), button1 )
        pygame.draw.rect( screen, (255, 0, 0), button2 )

        textsurface = button_font.render( 'Start', False, (255, 255, 255) )
        screen.blit(textsurface,( 220, 410 ))

        textsurface = button_font.render( 'Exit', False, (255, 255, 255) )
        screen.blit(textsurface,( 525, 410 ))

        click = False


        for event in pygame.event.get(): # Event Listener
            keys = pygame.key.get_pressed()
                    
            if event.type == pygame.QUIT:
                pygame.display.quit()
                pygame.quit()
                sys.exit(); #sys.exit() if sys is imported
                            
            elif event.type == pygame.KEYDOWN: # checks for keyboard input	
                pass

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True
            
                
        pygame.display.flip()
        clock.tick(60)



def home_menu():
    click = False

    pygame.mixer.music.set_volume( volume )
    pygame.mixer.music.load(home_screen_st)
    pygame.mixer.music.play(-1)
    
    while True:
        screen.fill( (0, 0, 0) )

        screen.blit( home_screen_bg, ( 0, 90 ) )
        
        pygame.font.init()
        myfont = pygame.font.SysFont('Comic Sans MS', 20)

        mx, my = pygame.mouse.get_pos()

        textsurface = myfont.render( 'Home', False, (255, 255, 255) )
        screen.blit(textsurface,( int(width/2-150), int(height/2-300) ))

        button1 = pygame.Rect( 200, 400, 125, 50 )

        if button1.collidepoint( (mx, my) ):
            if click:
                pygame.mixer.music.fadeout(fade_time)
                main_menu()

        pygame.draw.rect( screen, (0, 255, 100), button1 )
        textsurface = myfont.render( 'Title Screen', False, (255, 255, 255) )
        
        screen.blit(textsurface,( 200, 400 ))


        click = False


        for event in pygame.event.get(): # Event Listener
            keys = pygame.key.get_pressed()
                    
            if event.type == pygame.QUIT:
                pygame.display.quit()
                pygame.quit()
                sys.exit(); #sys.exit() if sys is imported
                            
            elif event.type == pygame.KEYDOWN: # checks for keyboard input	
                pass

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True
            
                
        pygame.display.flip()
        clock.tick(60)


def Quit():
    pygame.display.quit()
    pygame.quit()
    sys.exit(); #sys.exit() if sys is imported

main_menu()



while True:
    for event in pygame.event.get(): # Event Listener
        keys = pygame.key.get_pressed()
		
        if event.type == pygame.QUIT:
            Quit()
			
        elif event.type == pygame.KEYDOWN: # checks for keyboard input		
            #if keys[pygame.K_UP]:
            #    Greetings()
	#		
	#					
         #   elif keys[pygame.K_DOWN]:
          #      Goodbye()

            dialogue( dialogue_list )

    
