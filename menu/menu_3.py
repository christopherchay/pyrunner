import pygame, sys, time

pygame.init()

screen_width = 1000
screen_height = 700

screen = pygame.display.set_mode( (screen_width, screen_height) )

clock = pygame.time.Clock()

path_to = "../resources/music/"

## MUSIC
game_ost = path_to+"manaplus/icecontinent.ogg"
home_screen_st = path_to+"manaplus/8bit_the_hero.ogg"


## SFX
button_hover = path_to+"sfx/paper-update-cut.mp3"
click_sound = path_to+"sfx/notif-bell.ogg"


## WALLPAPERS



## IMAGES



## SETTING VARIABLES
volume = 1
fade_time = 500 #ms

pygame.mixer.music.set_volume( volume )

## COLORS
black = ( 0, 0, 0 )
white = ( 255, 255, 255 )
red = ( 255, 0, 0 )
light_red = ( 255, 125, 0 )
green = ( 0, 255, 0 )
light_green = ( 0, 255, 255 )
blue = ( 0, 0, 255 )
light_blue = ( 0, 125, 255 )


class hero:
    def __init__( self, name, st ):
        self.__name = name
        self.__st = st
        
        self.__hp = 100

        self.__strength = 1
        self.__speed = 1
        self.__dexterity = 1
        self.__agility = 1
        self.__vitality = 1

    def getName( self ):
        return self.__name
    def setName( self, name ):
        self.__name = name

    def getSt( self ):
        return self.__st
    def setSt( self, st ):
        self.__st = st

    def getHp( self ):
        return self.__hp
    def setHp( self, hp ):
        self.__hp = hp

    def getStrength( self ):
        return self.__strength
    def setStrength( self, strength ):
        self.__strength = strength

    def getSpeed( self ):
        return self.__speed
    def setSpeed( self, speed ):
        self.s__peed = speed

    def getDexterity( self ):
        return self.__dexterity
    def setDexterity( self, dexterity ):
        self.__dexterity = dexterity

    def getAgility( self ):
        return self.__agility
    def setAgility( self, agility ):
        self.__agility = agility

    def getVitality( self ):
        return self.__vitality
    def setVitality( self, vitality ):
        self.__vitality = vitality


def exitProgram():
    pygame.display.quit()
    pygame.quit()
    sys.exit(); #sys.exit() if sys is imported


def text_objects(text, font):
    textSurface = font.render(text, True, black)
    return textSurface, textSurface.get_rect()

def button( msg, x, y, width, height, initial_color, action_color, action=None ):
    mouse_pos = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x+width > mouse_pos[0] > x and y+height > mouse_pos[1] > y:
        pygame.draw.rect( screen, action_color, (x, y, width, height) )
        #pygame.mixer.music.load( button_hover )
        #pygame.mixer.music.play(1)
        
        if click[0] == 1 and action != None:
            #pygame.mixer.music.load( click_sound )
            pygame.mixer.music.play(1)
            action()
    else:
        pygame.draw.rect( screen, initial_color, (x, y, width, height) )

    smallText = pygame.font.SysFont( "Comic Sans MS", 20 )
    textSurf, textRect = text_objects( msg, smallText )
    textRect.center = ( round( x+(width/2) ), round( y+(height/2) ) )
    screen.blit( textSurf, textRect )
    

def heroButton( msg, x, y, width, height, initial_color, action_color, action=None, param=None, sound=None ):
    mouse_pos = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x+width > mouse_pos[0] > x and y+height > mouse_pos[1] > y:
        pygame.draw.rect( screen, action_color, (x, y, width, height) )
        #pygame.mixer.music.load( button_hover )
        #pygame.mixer.music.play(1)
        
        if click[0] == 1 and action != None:
            #pygame.mixer.music.load( click_sound )
            #pygame.mixer.music.play(1)
            action( param )
    else:
        pygame.draw.rect( screen, initial_color, (x, y, width, height) )

    smallText = pygame.font.SysFont( "Comic Sans MS", 20 )
    textSurf, textRect = text_objects( msg.getName(), smallText )
    textRect.center = ( round( x+(width/2) ), round( y+(height/2) ) )
    screen.blit( textSurf, textRect )


def titleScreen():
    #print("Loading Title Screen")
    
    click = False

    pygame.font.init()
    
    pygame.mixer.music.set_volume( volume )
    pygame.mixer.music.load(game_ost)
    pygame.mixer.music.play(-1)

    while True:
        screen.fill( black )
        
        for event in pygame.event.get():
            keys = pygame.key.get_pressed()

            if event.type == pygame.QUIT:
                exitProgram()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True
                    

        #Title
        title_font = pygame.font.SysFont('Comic Sans MS', 80)        
        text_surface = title_font.render( 'DAWN', False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 150 ) )
        screen.blit( text_surface, text_rect )

        #Sub-title
        title_font = pygame.font.SysFont('Comic Sans MS', 25)        
        text_surface = title_font.render( 'Mark of a New Day', False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 250 ) )
        screen.blit( text_surface, text_rect )

        #Version
        sub_font = pygame.font.SysFont('Comic Sans MS', 20)
        textsurface = sub_font.render( 'v0.3', False, (255, 255, 255) )
        screen.blit(textsurface,( screen_width-80, screen_height-225 ))

        #Buttons
        button( "Start", 25, 450, 200, 50, green, light_green, homeScreen )
        button( "Options", 25, 510, 200, 50, green, light_green )
        button( "Achievements", 25, 570, 200, 50, green, light_green )

        
        

        pygame.display.update()
        clock.tick(60)


def homeScreen():
    #print("Loading Home Screen")
    
    click = False

    pygame.font.init()
    
    pygame.mixer.music.set_volume( volume )
    pygame.mixer.music.load(home_screen_st)
    pygame.mixer.music.play(-1)

    while True:
        screen.fill( black )
        
        for event in pygame.event.get():
            keys = pygame.key.get_pressed()

            if event.type == pygame.QUIT:
                exitProgram()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True


        #Title
        title_font = pygame.font.SysFont('Comic Sans MS', 80)        
        text_surface = title_font.render( 'Home', False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 100 ) )
        screen.blit( text_surface, text_rect )

        #sub-Title
        title_font = pygame.font.SysFont('Comic Sans MS', 40)        
        text_surface = title_font.render( 'Team:', False, (255, 255, 255) )
        screen.blit( text_surface, ( 50, 170 ) )

        #Buttons
        button( "Return", 25, 625, 200, 50, red, light_red, titleScreen )

            ## TEAM

        team = [ hero1, hero2, hero3, hero4 ]
        pos = [ 50, 200, 350, 500 ]

        #team[team.index("Adam")] = "Blake"

        #for i in range( len(team) ):
        #    hero = team[i]
        #    heroButton( hero, pos[i], 235, 100, 50, blue, light_blue, characterScreen, hero )


        heroButton( hero1, pos[0], 235, 100, 50, blue, light_blue, characterScreen, hero1 )
        heroButton( hero2, pos[1], 235, 100, 50, blue, light_blue, characterScreen, hero2 )
        heroButton( hero3, pos[2], 235, 100, 50, blue, light_blue, characterScreen, hero3 )
        heroButton( hero4, pos[3], 235, 100, 50, blue, light_blue, characterScreen, hero4 )



        clock.tick(60)
        pygame.display.update()


def characterScreen( hero ):
    #print("Loading Hero Screen")
    
    click = False

    pygame.font.init()
    
    pygame.mixer.music.set_volume( volume-0.25 )
    pygame.mixer.music.load(hero.getSt())
    pygame.mixer.music.play(-1)

    while True:
        screen.fill( black )
        
        for event in pygame.event.get():
            keys = pygame.key.get_pressed()

            if event.type == pygame.QUIT:
                exitProgram()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        #Title
        title_font = pygame.font.SysFont('Comic Sans MS', 80)   
        text_surface = title_font.render( hero.getName(), False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 100 ) )
        screen.blit( text_surface, text_rect )

        #Sub-title
        title_font = pygame.font.SysFont('Comic Sans MS', 20)        
        text_surface = title_font.render( 'Hero Menu and Stats', False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 175 ) )
        screen.blit( text_surface, text_rect )

        #Hero Stats
        title_font = pygame.font.SysFont('Comic Sans MS', 30)
        text_surface = title_font.render( 'Health: ' + str(hero.getHp()), False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 225 ) )
        screen.blit( text_surface, text_rect )

        title_font = pygame.font.SysFont('Comic Sans MS', 25)
        text_surface = title_font.render( 'Speed: ' + str(hero.getSpeed()), False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 275 ) )
        screen.blit( text_surface, text_rect )

        text_surface = title_font.render( 'Strength: ' + str(hero.getStrength()), False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/3), 325 ) )
        screen.blit( text_surface, text_rect )

        text_surface = title_font.render( 'Dexterity: ' + str(hero.getDexterity()), False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round((screen_width/3)*2), 325 ) )
        screen.blit( text_surface, text_rect )

        text_surface = title_font.render( 'Vitality: ' + str(hero.getVitality()), False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/3), 375 ) )
        screen.blit( text_surface, text_rect )

        text_surface = title_font.render( 'Agility: ' + str(hero.getAgility()), False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round((screen_width/3)*2), 375 ) )
        screen.blit( text_surface, text_rect )

        #Buttons
        button( "Return", 25, 575, 200, 50, red, light_red, homeScreen )


        clock.tick(60)
        pygame.display.update()
        

hero1 = hero( "Adam", path_to+"manaplus/mvrasseli_nochains.ogg" )
hero1.setStrength( 4 )

hero2 = hero( "Eve", path_to+"manaplus/faith.ogg" )
hero2.setDexterity( 3 )
hero2.setHp( round(hero2.getHp()*0.85) )

hero3 = hero( "James", path_to+"manaplus/dragon_and_toast.ogg" )
hero3.setVitality( 3 )
hero3.setHp( round(hero3.getHp()*1.15) )

hero4 = hero( "Oscar", path_to+"manaplus/explorersmelody.ogg" )
hero4.setAgility( 4 )


titleScreen()
