"""
Write the Getter and Setter for any class 

Author: Christopher Chay
"""


getters = input( "Getters and Setters Here (Seperate with Space): " )

getterList = getters.split(" ")

for i in getterList:
	print( """
def Get{0}( self ):
	return self.__{0}
def Set{0}( self, {1} ):
	self.__{0} = {1}
		""" .format( i, i.lower() ) )

