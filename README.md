# PyRunner

Pygame tests and game development

**Main Program:**

- **map/map.py**
    - **Todo**
        - v.08 Quest Test
            - [ ]  Implement quest and npc

        - v.07 RPG Elements
            - [ ]  Add experience system
            - [ ]  Add leveling system
            - [ ]  Add player stats

    - **Released**
    - v.06.1
        - Finalized Save System!
        - Fixed Battle Display
        - Created Respawn Function
        -  Squashed many bugs!!
    - v.06
        - Create a save system
        - Incorporate existing modules into Game class
    - v.05
        - Add Monster Respawn Function
        - Add Max Monster Limit
        - Add Random Num of Starting Monsters
        - Optimize Volume toggle ( toggle does not work reliably )

    - v.04 
        - Battle system fully implemented
        - Integrated Classes for player and the game
        - Implemented Options Screen with volume toggle
        
    - v.03
        - Incorporated Monsters
        - File organization

    - v.02
        - Navigation implemented
        - Collision Detection implemented
        - Pop-up Box for Collision Alert implemented

    - v.01
        - Working Menus: ( Title Screen, Navigation Screen )
        - Placeholder Option Screen
