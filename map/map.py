import pygame, sys, random, time
import json

pygame.init()


## DISPLAY
screen_width = 1000
screen_height = 600

map_width = (200, 800)
map_height = (100, 500)

screen = pygame.display.set_mode( (screen_width, screen_height) )

clock = pygame.time.Clock()

path_to = "..\\resources\\"


## MUSIC
game_ost = path_to+"music\\manaplus\\despair.ogg"
navigation_st = path_to+"music\\manaplus\\misty_shrine.ogg"
battle_st = path_to+"music\\manaplus\\let_the_battles_begin.ogg"


## SFX
button_hover = path_to+"sfx\\paper-update-cut.mp3"
click_sound = path_to+"sfx\\notif-bell.ogg"


## IMAGES
hero_img = pygame.image.load(path_to+"images\\rubikon.png")
monster_img = pygame.image.load(path_to+"images\\tezer.png")


## SETTING VARIABLES
#volume = 5.0
fade_time = 500 #ms


## COLORS
black = ( 0, 0, 0 )
white = ( 255, 255, 255 )
red = ( 255, 0, 0 )
light_red = ( 255, 125, 0 )
green = ( 0, 255, 0 )
light_green = ( 0, 255, 255 )
blue = ( 0, 0, 255 )
light_blue = ( 0, 125, 255 )
yellow = ( 255, 255, 0 )


pygame.font.init()
'''
try:
    with open( "../data/game_data.json", "w" ) as game_data_file:
        game_data = json.load( game_data_file )
except:
    game_data = {
        "volume": 5.0,
        "combat_speed": 1,
        "monster_spawn_limit": 5,
        "alive_monster": []
    }
'''


class Game():
    def __init__( self, game_data ):
        self.__data = game_data

    def getData( self ):
        return self.__data

    def getVolume( self ):
        return self.__data["volume"]
    def setVolume( self, volume ):
        self.__data["volume"] = volume

    def getCombatSpeed( self ):
        return self.__data["combat_speed"]
    def setCombatSpeed( self, speed ):
        self.__data["combat_speed"] = speed

    def getMonsterSpawnLimit( self ):
        return self.__data["monster_spawn_limit"]
    def setMonsterSpawnLimit( self, limit ):
        self.__data["monster_spawn_limit"] = limit

    def getAliveMonsters( self ):
        return self.__data["alive_monster"]
    def setAliveMonsters( self, monster_list ):
        self.__data["alive_monster"] = monster_list
    def clearAliveMonsters( self ):
        self.__data["alive_monster"] = []
    def addMonster( self, monster ):
        self.__data["alive_monster"].append(monster)
    def removeMonster( self, monster ):
        self.__data["alive_monster"].remove( monster )

    


#game = Game( game_data )


class Hero:
    def __init__( self, player_data):
        self.__data = player_data

    def getData( self ):
        return self.__data

    def getStatus( self ):
        return self.__data["status"]
    def setStatus( self, status ):
        self.__data["status"] = status

    def getName( self ):
        return self.__data["name"]
    def setName( self, name ):
        self.__data["name"] = name

    def getMaxHp( self ):
        return self.__data["maxhp"]
    def setMaxHp( self, maxhp ):
        self.__data["maxhp"] = maxhp

    def getHp( self ):
        return self.__data["hp"]
    def setHp( self, hp ):
        self.__data["hp"] = hp

    def getPos( self ):
        return self.__data["pos"]
    
    def setPosX( self, pos ):
       self.__data["pos"][0] = pos
    def setPosY( self, pos ):
        self.__data["pos"][1] = pos

    def getMovement( self ):
        return self.__data["movement"]


def randLocation( map_width, map_height ):
    """return list with map param"""

    rand_x = random.randrange( map_width[0], map_width[1], 25 )
    rand_y = random.randrange( map_height[0], map_height[1], 25 )

    return [rand_x, rand_y]

## ADD MONSTERS TO ALIVE MONSTER LIST


def genRandMonster( game_engine, player_data ):
    random_hp = random.randint( 20, 35 )
    random_location = randLocation(map_width, map_height)

    monster_data = { "name": "", "hp": 0, "pos": [0,0], "movement": 25 }

    #print( monster_data["name"] )

    monster_data["name"] = random.choice( ("Goblin", "Orc", "Terranite") )
    monster_data["hp"] = random_hp
    monster_data["pos"] = random_location

    game_engine.addMonster( Hero( monster_data  ) )


def getNewSpawn( game_engine, player_data ):
    game_engine.clearAliveMonsters()

    for i in range(game_engine.getMonsterSpawnLimit()):
        genRandMonster( game_engine, player_data )  


def saveAliveMonsters( monster_list ):
    '''Break down the list of class objects to dictionaries to save'''
    temp_list = []
    for monster in monster_list:
        temp_list.append( monster.getData() )
        #print("monster")

    return temp_list


def exitProgram( game, player ):
    game.setAliveMonsters( saveAliveMonsters( game.getAliveMonsters() ) )

    try:
        with open( "../data/game_data.json", "w" ) as game_data_file:
            json.dump( game.getData(), game_data_file, indent=4 )

        with open( "../data/player_data.json", "w" ) as player_data_file:
            json.dump( player.getData(), player_data_file, indent=4 )
    except TypeError as e:
        print( e )

    pygame.display.quit()
    pygame.quit()
    sys.exit()


def drawObject( position, symbol, color ):
    font = pygame.font.SysFont( "Comic Sans Ms", 25 )
    text_surface = font.render( symbol, False, (color) )
    text_rect = text_surface.get_rect( center=(position[0], position[1]) )
    screen.blit( text_surface, text_rect )


def drawBorders(screen, x, y, width, height, border_width, color):
    pygame.draw.rect(screen, color, (x, y, width, height), border_width)


def isLegalMove( player, direction ):
    if direction == "n":
        if player.getPos()[1] - player.getMovement() < map_height[0]:
            return False
    elif direction == "s":
        if player.getPos()[1] + player.getMovement() > map_height[1]:
            return False
    elif direction == "e":
        if player.getPos()[0] - player.getMovement() < map_width[0]:
            return False
    elif direction == "w":
        if player.getPos()[0] + player.getMovement() > map_width[1]:
            return False

    return True


def textObjects(text, font):
    text_surface = font.render(text, True, black)
    return text_surface, text_surface.get_rect()


def button( msg, x, y, width, height, initial_color, action_color, action=None, param=None):
    mouse_pos = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x+width > mouse_pos[0] > x and y+height > mouse_pos[1] > y:
        pygame.draw.rect( screen, action_color, (x, y, width, height) )
        #pygame.mixer.music.load( button_hover )
        #pygame.mixer.music.play(1)
        
        if click[0] == 1:
            pygame.mixer.music.load( click_sound )
            pygame.mixer.music.play(1)
            #Action paramter check!
            time.sleep(0.15)

            if action != None:
                if len(param) == 1:
                    action( param[0] )
                elif len(param) == 2:
                    action( param[0], param[1] )
                elif len(param) == 3:
                    action( param[0], param[1], param[2] )
                elif len(param) == 4:
                    action( param[0], param[1], param[2], param[3] )

    else:
        pygame.draw.rect( screen, initial_color, (x, y, width, height) )

    smallText = pygame.font.SysFont( "Comic Sans MS", 20 )
    textSurf, textRect = textObjects( msg, smallText )
    textRect.center = ( round( x+(width/2) ), round( y+(height/2) ) )
    screen.blit( textSurf, textRect )


def popUp( text, x, y, width, height, color ):
    pygame.draw.rect( screen, color, (x, y, width, height) )

    font = pygame.font.SysFont( "Comic Sans MS", 20 )
    text_surf, text_rect = textObjects( text, font )
    text_rect.center = ( round(x+(width/2)), round(y+(height/2)) )
    screen.blit( text_surf, text_rect )


def hpColor( max_hp, hp ):
    if hp >= round(max_hp*0.7): #70%
        return green
    elif hp >= round(max_hp*0.4) and hp < round(max_hp*0.7): # 40% < hp < 70%
        return yellow
    else:
        return red


def collisionMsg():
    return random.choice([
        "Yikes! Watch it!!",
        "Out of Bounds!!",
        "No Entrance!!",
        "Turn Around!!",
        "Thats the end of the map!!",
        "You can't go there!!"
        ])


def handleCollision( game, player ):
    for i in game.getAliveMonsters():
        if player.getPos() == i.getPos():
            battleScreen( game, player )

    #if player.getPos() in obstacles:
        #pass



def loadGame( game, player ): # Currenly, this function will only convert monster dictionaries to objects
    temp_list = []
    for monster_data in game.getAliveMonsters():
        temp_list.append( Hero(monster_data) )

    game.setAliveMonsters( temp_list )

    #print( game.getAliveMonsters() )


def titleScreen( game, player ):
    #print("Loading Title Screen")
    
    click = False

    pygame.font.init()
    
    #print( "volume:", game.getVolume() )
    pygame.mixer.music.set_volume( game.getVolume() )
    pygame.mixer.music.load(game_ost)
    pygame.mixer.music.play(-1)

    while True:
        screen.fill( black )
        
        for event in pygame.event.get():
            keys = pygame.key.get_pressed()

            if event.type == pygame.QUIT:
                exitProgram( game, player )

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True
                    time.sleep(0.1)
                    

        #Title
        title_font = pygame.font.SysFont('Calibri', 80)        
        text_surface = title_font.render( 'Navigation', False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 150 ) )
        screen.blit( text_surface, text_rect )

        #Sub-title
        title_font = pygame.font.SysFont('Calibri', 25)        
        text_surface = title_font.render( 'Left, Right Up and Down', False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 250 ) )
        screen.blit( text_surface, text_rect )

        #Creator
        title_font = pygame.font.SysFont('Calibri', 20)        
        text_surface = title_font.render( 'By Christopher Chay', False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 300 ) )
        screen.blit( text_surface, text_rect )

        #Update Message!
        title_font = pygame.font.SysFont('Calibri', 40, bold=True)        
        text_surface = title_font.render( 'Save System Update Part2!!!', False, (255, 255, 255) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 400 ) )
        #screen.blit( text_surface, text_rect )

        message = pygame.transform.rotate( text_surface, 20 )
        screen.blit( message, text_rect )

        #Version
        sub_font = pygame.font.SysFont('Calibri', 20)
        textsurface = sub_font.render( 'v06.1', False, (255, 255, 255) )
        screen.blit(textsurface,( screen_width-80, screen_height-125 ))

        #Buttons
        button( "Start", 25, 400, 200, 50, green, light_green, navigation, [game, player] )
        button( "Options", 25, 460, 200, 50, green, yellow, options, [game, player] )
        button( "Quit", 25, 520, 200, 50, green, light_red, exitProgram, [game, player] )

        
        

        pygame.display.update()
        clock.tick(60)


def options( game, player ):
    click = False
    
    while True:
        screen.fill( black )

        for event in pygame.event.get():
            keys = pygame.key.get_pressed()

            if event.type == pygame.QUIT:
                exitProgram( game_data )

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True
                    time.sleep(0.1)


        ## VOLUME
        if game.getVolume() == 0:
            button( "Volume: off", 25, 400, 200, 50, green, light_green, volumeToggle, [game] )
        else:
            button( "Volume: on", 25, 400, 200, 50, red, light_red, volumeToggle, [game] )


        ## COMBAT SPEED

        if game.getCombatSpeed() == 1:
            button( "Combat Speed: Me", 25, 460, 200, 50, yellow, light_green, combatSpeedToggle, [game] )

        elif game.getCombatSpeed() == 0.5:
            button( "Combat Speed: Hi", 25, 460, 200, 50, green, light_green, combatSpeedToggle, [game] )

        elif game.getCombatSpeed() == 2:
            button( "Combat Speed: Lo", 25, 460, 200, 50, red, light_green, combatSpeedToggle, [game] )

        button( "Return", 25, 520, 200, 50, red, light_red, titleScreen, [game, player] )

        pygame.display.update()
        clock.tick(60)



def volumeToggle( game ):
    if game.getVolume() == 0:
        game.setVolume( 0.5 )
    else:
        game.setVolume( 0 )
                        

def combatSpeedToggle( game ):
    if game.getCombatSpeed() == 1:
        game.setCombatSpeed( 0.5 )
    elif game.getCombatSpeed() == 0.5:
        game.setCombatSpeed( 2 )
    else:
        game.setCombatSpeed( 1 )


def navigation( game, player ):
    pygame.mixer.music.set_volume( game.getVolume()/2 )
    pygame.mixer.music.load(navigation_st)
    pygame.mixer.music.play(-1)
    
    collision = False
    click = False
    
    while True:
        screen.fill( black )

        for event in pygame.event.get():
            keys = pygame.key.get_pressed()

            if event.type == pygame.QUIT:
                exitProgram( game, player )

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True
                    time.sleep(0.1)

            if event.type == pygame.KEYDOWN:
                if len(game.getAliveMonsters()) < game.getMonsterSpawnLimit():
                    if random.randint(1, 6) == 1: ## 1 in 6 chance of monster respaning
                        genRandMonster( game, player )

                if keys[pygame.K_UP]:
                    if isLegalMove( player, "n" ):
                        player.setPosY(player.getPos()[1] - player.getMovement())
                        collision = False
                    else:
                        collision = True
                        c_message = collisionMsg()
                        color = light_red
                    
                elif keys[pygame.K_DOWN]:
                    if isLegalMove( player, "s" ):
                        player.setPosY(player.getPos()[1] + player.getMovement())
                        collision = False
                    else:
                        collision = True
                        c_message = collisionMsg()
                        color = light_red
                
                elif keys[pygame.K_LEFT]:
                    if isLegalMove( player, "e" ):
                        player.setPosX(player.getPos()[0] - player.getMovement())
                        collision = False
                    else:
                        collision = True
                        c_message = collisionMsg()
                        color = light_red
                
                elif keys[pygame.K_RIGHT]:
                    if isLegalMove( player, "w" ):
                        player.setPosX(player.getPos()[0] + player.getMovement())
                        collision = False
                    else:
                        collision = True
                        c_message = collisionMsg()
                        color = light_red



        #Title
        title_font = pygame.font.SysFont('Calibri', 80)        
        text_surface = title_font.render( 'Welcome to the MAP', False, (blue) )
        text_rect = text_surface.get_rect( center=( round(screen_width/2), 50 ) )
        screen.blit( text_surface, text_rect )

        #Buttons
        button( "Title Screen", round(screen_width/2)-100, screen_height-65, 200, 50, red, light_red, titleScreen, [game, player] )

        #draw player
        drawObject( player.getPos(), "H", blue )
        drawBorders( screen, map_width[0]-15, map_height[0]-15, map_width[1]-map_width[0]+30, map_height[1]-map_height[0]+30, 10, white )
        #print( player.getPos() )

        #draw animation
        #popUp( "", 0, screen_height-200, 300, 200, black )

        #print( player.getStatus() )

        ## Respawn the player!
        if player.getStatus() == "dead":
            player.setHp( player.getMaxHp() )
            player.setPosX( round(screen_width/2) )
            player.setPosY( round(screen_height/2) )
            player.setStatus( "alive" )


        # MONSTER LOGIC
        for i in game.getAliveMonsters():
            drawObject( i.getPos(), "M", light_red )

        for i in  game.getAliveMonsters():
            if player.getPos() == i.getPos():
                collision = True
                c_message = "!! Monster Encounter !!"
                color = red
            

        if collision:
            popUp( c_message, screen_width-300, screen_height-200, 300, 200, color )
            handleCollision( game, player )


        pygame.display.update()
        clock.tick(60)


def battleScreen( game, player ):
    pygame.mixer.music.set_volume( game.getVolume()-0.2 )
    pygame.mixer.music.load(battle_st)
    pygame.mixer.music.play(-1)

    conflict_resolved = False
    click = False
    init = True
    
    while True:
        screen.fill( black )

        for event in pygame.event.get():
            keys = pygame.key.get_pressed()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        #Title
        title_font = pygame.font.SysFont('Calibri', 60, bold=True)        
        text_title_surface = title_font.render( 'BATTLE', False, (red) )
        text_rect = text_title_surface.get_rect( center=( round(screen_width/2), 75 ) )
        screen.blit( text_title_surface, text_rect )

        drawBorders( screen, 5, screen_height-150, screen_width-10, 150-5, 10, white )

        if conflict_resolved:
            if player.getStatus() == "alive":
                battleDisplay(player, monster)

                title_font = pygame.font.SysFont('Calibri', 60, bold=True)        
                text_surface = title_font.render( 'You Win!', False, (white) )
                text_rect = text_surface.get_rect( center=( round(screen_width/2), 200 ) )
                screen.blit( text_surface, text_rect )

                try:
                    game.removeMonster( monster ) ## Remove monster if it is dead
                except:
                    pass

                button( "Continue", round(screen_width/2)-100, 400, 200, 50, green, light_green, navigation, [game, player] )
            
            else:
                battleDisplay(player, monster)

                title_font = pygame.font.SysFont('Calibri', 60, bold=True)        
                text_surface = title_font.render( 'You Lost!', False, (white) )
                text_rect = text_surface.get_rect( center=( round(screen_width/2), 200 ) )
                screen.blit( text_surface, text_rect )

                ## For now, when you die, you can respawn at the center with full hp
                ## Later on, there needs to be an exp malus or gold malus every time you die
                ## Come back to this

                button( "Respawn", round(screen_width/2)-100, 400, 200, 50, green, light_green, navigation, [game, player] )



        pygame.display.update()
        clock.tick(60)



        # Battle Display
        while not conflict_resolved:
            screen.fill( black, ( 0, 175, screen_width, 270 ) )

            for event in pygame.event.get():
                keys = pygame.key.get_pressed()

                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        click = True

             #Find the monster
            for i in  game.getAliveMonsters():
                if player.getPos() == i.getPos():
                    monster = i


            battleDisplay( player, monster )


            if init:
                time.sleep(0.5)
                init = False


            ## Battle Function
            if player.getHp() > 0 and monster.getHp() > 0:
                if random.randint( 1, 10 ) > random.randint( 1, 6 ):
                    damage = random.randint( 1, 6 )
                    battleIndicator( "-" + str(damage), red, monster.getName() )
                    monster.setHp( monster.getHp() - damage )
                else:
                    battleIndicator( "Missed!!", white, monster.getName() )

                if random.randint( 1, 6 ) > random.randint( 1, 6 ):
                    damage = random.randint( 1, 3 )
                    battleIndicator( "-" + str(damage), red, player.getName() )
                    player.setHp( player.getHp() - damage )
                else:
                    battleIndicator( "Missed!!", white, player.getName() )


            if player.getHp() <= 0 or monster.getHp() <= 0:
                conflict_resolved = True

                if player.getHp() <= 0:
                    player.setStatus( "dead" )

            if conflict_resolved:
                break


            pygame.display.update()
            clock.tick(60)
            time.sleep(game.getCombatSpeed())

def battleDisplay( player, monster ):
    title_font = pygame.font.SysFont('Calibri', 60, bold=True)
    text_surface = title_font.render( 'BATTLE', False, (red) )
    #Hero Display
    image_rect = text_surface.get_rect( center=( round(screen_width/3), 275 ) )
    screen.blit( hero_img, image_rect )
            #hp
    title_font = pygame.font.SysFont('Comic Sans MS', 20, bold=True)        
    text_surface = title_font.render( "Hp: "+str(player.getHp()), False, hpColor(player.getMaxHp(), player.getHp()) )
    text_rect = text_surface.get_rect( center=( round(screen_width/3)-round(hero_img.get_width()*(1/3)), 225 ) )
    screen.blit( text_surface, text_rect )
            #name
    title_font = pygame.font.SysFont('Comic Sans MS', 20, bold=True)        
    text_surface = title_font.render( player.getName(), False, (255, 255, 255) )
    text_rect = text_surface.get_rect( center=( round(screen_width/3)-round(hero_img.get_width()*(1/3)), 425 ) )
    screen.blit( text_surface, text_rect )
            
          
    if monster.getHp() > 0:
        image_rect = text_surface.get_rect( center=( round(screen_width*(2/3)), 275 ) )
        screen.blit( monster_img, image_rect )
    else:
        image_rect = text_surface.get_rect( center=( round(screen_width*(2/3)), 275 ) )
        dead_monster = pygame.transform.rotate( monster_img, 90 )
        screen.blit( dead_monster, image_rect )
                
            #hp
    title_font = pygame.font.SysFont('Comic Sans MS', 20, bold=True)        
    text_surface = title_font.render( "Hp: "+str( monster.getHp() ), False, (255, 255, 255) )
    text_rect = text_surface.get_rect( center=( round(screen_width*(2/3))+round(monster_img.get_width()*(1/3)), 225 ) )
    screen.blit( text_surface, text_rect )
            #name
    title_font = pygame.font.SysFont('Comic Sans MS', 20, bold=True)        
    text_surface = title_font.render( monster.getName(), False, (255, 255, 255) )
    text_rect = text_surface.get_rect( center=( round(screen_width*(2/3))+round(monster_img.get_width()*(1/3)), 425 ) )
    screen.blit( text_surface, text_rect )


    

def battleIndicator( msg, color, target ):
    if target == "Steve":
        place = 1/3
        width = hero_img.get_width()*(1/3)
        width = round(screen_width*(place))-round(width)
    else:
        place = 2/3
        width = monster_img.get_width()*(1/3)
        width = round(screen_width*(place))+round(width)
    
    title_font = pygame.font.SysFont('Comic Sans MS', 20, bold=True)        
    text_surface = title_font.render( msg, False, color )
    text_rect = text_surface.get_rect( center=( width, 200 ) )
    screen.blit( text_surface, text_rect )



